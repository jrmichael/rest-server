package rest.server.controller;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class StorageController {

    public static final String ID = "id";

    Map<String, Map<String, Object>> storage = new ConcurrentHashMap<String, Map<String, Object>>();

    AtomicLong id = new AtomicLong();

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public Collection<Map<String, Object>> list() throws IOException {
        return storage.values();
    }

    @RequestMapping(value = "/", method = {RequestMethod.POST, RequestMethod.PUT}, consumes = "application/json")
    @ResponseBody
    public Map<String, Object> save(@RequestBody Map<String, Object> entity) throws IOException {
        String id = entity.containsKey(ID) ? entity.get(ID).toString() : String.valueOf(this.id.incrementAndGet());
        entity.put(ID, id);
        storage.put(id, entity);
        return entity;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> one(@PathVariable(ID) String id) throws IOException {
        return storage.get(id);
    }

    @RequestMapping(value = "/{id}", method = {RequestMethod.PUT, RequestMethod.POST})
    @ResponseBody
    public Map<String, Object> update(@RequestBody Map<String, Object> entity, @PathVariable(ID) String id) throws IOException {
        entity.put(ID, id);
        storage.put(id, entity);
        return entity;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable(ID) String id) throws IOException {
        storage.remove(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
